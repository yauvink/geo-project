import React from 'react';
import Map from './components/map';
import './App.css';

const App: React.FC = () => (
  <div className="App">
    <Map></Map>
  </div>
);

export default App;
